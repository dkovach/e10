#!/bin/bash

###########################################################################
#
# name: run_all.sh 
#
# purpose: run training and classification 
#
# usage: ./run_all.sh 
#
# description: runs training and classification with different
#  models and parameters.  Outputs results to 
#  Results/ directory
#
###########################################################################


# stochastic gradient boosting
echo "stochastic gradient boosting"
Rscript analysis.r --run --method "gbm" --pca > Results/gbm_pca.txt
Rscript analysis.r --run --method "gbm" > Results/gbm.txt

# bagged trees
echo "bagged trees"
Rscript analysis.r --run --method "treebag" --pca > Results/treebag_pca.txt
Rscript analysis.r --run --method "treebag" > Results/treebag.txt

# support vector machines with linear kernel
echo "support vector machines with linear kernel"
Rscript analysis.r --run --method "svmLinear" --pca > Results/svmLinear_pca.txt
Rscript analysis.r --run --method "svmLinear" > Results/svmLinear.txt

# Partial Least Squares
echo "Partial Least Squares"
Rscript analysis.r --run --method "pls" --pca > Results/pls_pca.txt
Rscript analysis.r --run --method "pls" > Results/pls.txt

# Quadratic Discriminant Analysis
echo "Quadratic Discriminant Analysis"
Rscript analysis.r --run --method  "qda" --pca > Results/qda_pca.txt
Rscript analysis.r --run --method  "qda" > Results/qda.txt

# Quadratic Discriminant Analysis with Stepwise Feature Selection
echo "Quadratic Discriminant Analysis with Stepwise Feature Selection"
Rscript analysis.r --run --method  "stepQDA" --pca > Results/stepQDA_pca.txt
Rscript analysis.r --run --method  "stepQDA" > Results/stepQDA.txt

# Random Forest
echo "Random Forest"
Rscript analysis.r --run --method  "ordinalRF" --pca > Results/ordinalRF_pca.txt
Rscript analysis.r --run --method  "ordinalRF" > Results/ordinalRF.txt

# Support Vector Machines with Class Weights
echo "Support Vector Machines with Class Weights"
Rscript analysis.r --run --method  "svmRadialWeights" --pca > Results/svmRadialWeights_pca.txt
Rscript analysis.r --run --method  "svmRadialWeights" > Results/svmRadialWeights.txt

# Tree Models from Genetic Algorithms
echo "Tree Models from Genetic Algorithms"
Rscript analysis.r --run --method  "evtree" --pca > Results/evtree_pca.txt
Rscript analysis.r --run --method  "evtree" > Results/evtree.txt

# AdaBoost Classification Trees
echo "AdaBoost Classification Trees"
Rscript analysis.r --run --method  "adaboost" --pca > Results/adaboost_pca.txt
Rscript analysis.r --run --method  "adaboost" > Results/adaboost.txt

# Adaptive Mixture Discriminant Analysis
echo "Adaptive Mixture Discriminant Analysis"
Rscript analysis.r --run --method  "amdai" --pca > Results/amdai_pca.txt
Rscript analysis.r --run --method  "amdai" > Results/amdai.txt

# Bagged MARS
echo "Bagged MARS"
Rscript analysis.r --run --method  "bagEarth" --pca > Results/bagEarth_pca.txt
Rscript analysis.r --run --method  "bagEarth" > Results/bagEarth.txt

# Bayesian Additive Regression Trees
echo "Bayesian Additive Regression Trees"
Rscript analysis.r --run --method  "bartMachine" --pca > Results/bartMachine_pca.txt
Rscript analysis.r --run --method  "bartMachine" > Results/bartMachine.txt




# Analyze results
ls Results/ | while read line; do echo $line; egrep "MD|   Accuracy" Results/$line | grep -v Kappa; done
