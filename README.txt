###########################################################################
#
# name			: README.txt 
#
# purpose		: provide information about the project 
#
# usage			: read it 
#
# description		:
#
###########################################################################


###########################################################################
# Introduction
###########################################################################


The purpose of this document is to provide information about the analysis.r
script which seeks to apply classification methods to the dataset provided.

Broadly, this can be broken down into three stages:
1. Data discovery
2. Training 
3. Classification

All meaningful functionality is contained in the analysis.r file.  To run, 
simply enter the following on the command line:

> Rscript analysis.r <options>

to run the analysis with multiple models, with different options simply 
run:

> chmod 777 run_all.sh
> ./run_all.sh

This will run training and classification with multiple models and output
the results to the Results/ directory.  It won't run ALL models from caret,
only my favorite ones.


###########################################################################
# Requirements 
###########################################################################


To run, we require the R programming language.  For information on how
to do this on your platform, visit

https://www.r-project.org

Note that analysis.r needs external R packages to run successfully.  The
program first checks to make sure each of these are installed.  If not,
it will attempt to install them for you.

** Note: on MacOSX the caret package requires the omp package to be 
installed first.  You can install it with homebrew:

> brew install libomp


###########################################################################
# Data Exploration
###########################################################################


To analyze the data with Principal Component Analysis, run the following
command:

> Rscript analysis.r --explore

This will output graphics of the transformed data, colored by Result, by 
MDTag.  It will graph the principal components in two and three dimensions.


###########################################################################
# Data Classification
###########################################################################


To run the analysis on the data, run the following:

> Rscript analysis.r --run --method <model name>

where 'model name' is the desired model from the caret package.  For a list
of available models, visit

https://topepo.github.io/caret/available-models.html

To run analysis.r with PCA applied to preprocess the data, add the '--pca'
argument as follows:

> Rscript analysis.r --run --method <model name> --pca

This will run the model on the first three principal components of the
transformed data


###########################################################################
# Data Classification - Steps
###########################################################################


First, we read in the data from the Data directory.  We then run the 
analysis on the data, separating it by MDTag.


Since no testing data was provided, we next divide the data set into 
training and testing sets.


Next, if the PCA arg is specified, we transform the data.  Note that 
special care was taken to keep the distinction between training and testing
sets.  Thus, PCA is applied to the testing set based on the model generated
by running PCA on the training set.  PCA is not run on the data as a whole.

The next step is the training phase.  Here, we first specify a method of
cross validation, and run the training based on the method name specified.

Finally, we apply the trained model to the testing data, and get performance
metrics by calling the 'confusionMatrix' function from the R package.


###########################################################################
# Results
###########################################################################


From Principal Component Analysis we can see that we can explain 99% of the
variance of the data with 3 Principal Components.  This is the reason why
three PC's were used in training and classification.

In all cases, PCA found High Speed Distance to have a low correlation to 
the other variables.

In all cases, PCA hurt our accuracy.  It seems that extra information 
content from the last variable was important enough to effect the results.

Bagged trees were by far the most accurate predictive model, with an 
accuracy of up to 87% in some cases.

The predictive accuracy varied by MDTag.  MD-2 and MD-5 seemed to provide 
the best accuracy.  MD-7 did not feature all outcomes, so this yielded
errors.


###########################################################################
# Future work
###########################################################################


All data projects can present 'black holes' with opportunities for further
research in terms of data preprocessing, tweaking parameters, using 
different models, and analyzing the results.  

Some opportunities for further analysis include:

1. Outputting results in a human readable manner to explain the parameters
   most optimal for winning.  I'd use a decision tree for this.
2. Deeper analysis of the results: using AUC/ROC curves, etc.
3. Using more models from the caret package and potentially others
