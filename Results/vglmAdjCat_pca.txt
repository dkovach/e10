[1] "MD -1"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  0  0
         L 19 65 32
         W  0  0  0

Overall Statistics
                                          
               Accuracy : 0.5603          
                 95% CI : (0.4652, 0.6524)
    No Information Rate : 0.5603          
    P-Value [Acc > NIR] : 0.5387          
                                          
                  Kappa : 0               
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.0000   1.0000   0.0000
Specificity            1.0000   0.0000   1.0000
Pos Pred Value            NaN   0.5603      NaN
Neg Pred Value         0.8362      NaN   0.7241
Prevalence             0.1638   0.5603   0.2759
Detection Rate         0.0000   0.5603   0.0000
Detection Prevalence   0.0000   1.0000   0.0000
Balanced Accuracy      0.5000   0.5000   0.5000
[1] "MD"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  0  0
         L 16 59 30
         W  0  0  0

Overall Statistics
                                          
               Accuracy : 0.5619          
                 95% CI : (0.4617, 0.6586)
    No Information Rate : 0.5619          
    P-Value [Acc > NIR] : 0.5407          
                                          
                  Kappa : 0               
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.0000   1.0000   0.0000
Specificity            1.0000   0.0000   1.0000
Pos Pred Value            NaN   0.5619      NaN
Neg Pred Value         0.8476      NaN   0.7143
Prevalence             0.1524   0.5619   0.2857
Detection Rate         0.0000   0.5619   0.0000
Detection Prevalence   0.0000   1.0000   0.0000
Balanced Accuracy      0.5000   0.5000   0.5000
[1] "MD -5"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction D L W
         D 8 3 0
         L 1 7 3
         W 0 0 1

Overall Statistics
                                          
               Accuracy : 0.6957          
                 95% CI : (0.4708, 0.8679)
    No Information Rate : 0.4348          
    P-Value [Acc > NIR] : 0.01044         
                                          
                  Kappa : 0.4905          
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.8889   0.7000  0.25000
Specificity            0.7857   0.6923  1.00000
Pos Pred Value         0.7273   0.6364  1.00000
Neg Pred Value         0.9167   0.7500  0.86364
Prevalence             0.3913   0.4348  0.17391
Detection Rate         0.3478   0.3043  0.04348
Detection Prevalence   0.4783   0.4783  0.04348
Balanced Accuracy      0.8373   0.6962  0.62500
[1] "MD -4"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  4  2  0
         L 13 23  4
         W  0  0  5

Overall Statistics
                                          
               Accuracy : 0.6275          
                 95% CI : (0.4808, 0.7587)
    No Information Rate : 0.4902          
    P-Value [Acc > NIR] : 0.03396         
                                          
                  Kappa : 0.3336          
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity           0.23529   0.9200  0.55556
Specificity           0.94118   0.3462  1.00000
Pos Pred Value        0.66667   0.5750  1.00000
Neg Pred Value        0.71111   0.8182  0.91304
Prevalence            0.33333   0.4902  0.17647
Detection Rate        0.07843   0.4510  0.09804
Detection Prevalence  0.11765   0.7843  0.09804
Balanced Accuracy     0.58824   0.6331  0.77778
[1] "MD -2"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  1  2  0
         L  4 14  2
         W  0  2  6

Overall Statistics
                                          
               Accuracy : 0.6774          
                 95% CI : (0.4863, 0.8332)
    No Information Rate : 0.5806          
    P-Value [Acc > NIR] : 0.182           
                                          
                  Kappa : 0.4061          
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity           0.20000   0.7778   0.7500
Specificity           0.92308   0.5385   0.9130
Pos Pred Value        0.33333   0.7000   0.7500
Neg Pred Value        0.85714   0.6364   0.9130
Prevalence            0.16129   0.5806   0.2581
Detection Rate        0.03226   0.4516   0.1935
Detection Prevalence  0.09677   0.6452   0.2581
Balanced Accuracy     0.56154   0.6581   0.8315
[1] "MD -3"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  7  0  1
         L  4 14  5
         W  1  3  7

Overall Statistics
                                          
               Accuracy : 0.6667          
                 95% CI : (0.5045, 0.8043)
    No Information Rate : 0.4048          
    P-Value [Acc > NIR] : 0.0005421       
                                          
                  Kappa : 0.4815          
                                          
 Mcnemar's Test P-Value : 0.2122903       

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.5833   0.8235   0.5385
Specificity            0.9667   0.6400   0.8621
Pos Pred Value         0.8750   0.6087   0.6364
Neg Pred Value         0.8529   0.8421   0.8065
Prevalence             0.2857   0.4048   0.3095
Detection Rate         0.1667   0.3333   0.1667
Detection Prevalence   0.1905   0.5476   0.2619
Balanced Accuracy      0.7750   0.7318   0.7003
[1] "MD -7"
--> [ 0 ] Transform data with PCA
