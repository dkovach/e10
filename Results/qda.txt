[1] "MD -1"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  4  1
         L 19 58 30
         W  0  3  1

Overall Statistics
                                          
               Accuracy : 0.5086          
                 95% CI : (0.4142, 0.6026)
    No Information Rate : 0.5603          
    P-Value [Acc > NIR] : 0.8877          
                                          
                  Kappa : -0.0532         
                                          
 Mcnemar's Test P-Value : 3.425e-07       

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.0000  0.89231 0.031250
Specificity            0.9485  0.03922 0.964286
Pos Pred Value         0.0000  0.54206 0.250000
Neg Pred Value         0.8288  0.22222 0.723214
Prevalence             0.1638  0.56034 0.275862
Detection Rate         0.0000  0.50000 0.008621
Detection Prevalence   0.0431  0.92241 0.034483
Balanced Accuracy      0.4742  0.46576 0.497768
[1] "MD"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  1  0
         L 15 52 23
         W  1  6  7

Overall Statistics
                                          
               Accuracy : 0.5619          
                 95% CI : (0.4617, 0.6586)
    No Information Rate : 0.5619          
    P-Value [Acc > NIR] : 0.5407          
                                          
                  Kappa : 0.0851          
                                          
 Mcnemar's Test P-Value : 3.641e-05       

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity          0.000000   0.8814  0.23333
Specificity          0.988764   0.1739  0.90667
Pos Pred Value       0.000000   0.5778  0.50000
Neg Pred Value       0.846154   0.5333  0.74725
Prevalence           0.152381   0.5619  0.28571
Detection Rate       0.000000   0.4952  0.06667
Detection Prevalence 0.009524   0.8571  0.13333
Balanced Accuracy    0.494382   0.5276  0.57000
[1] "MD -5"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction D L W
         D 8 0 0
         L 1 9 3
         W 0 1 1

Overall Statistics
                                         
               Accuracy : 0.7826         
                 95% CI : (0.563, 0.9254)
    No Information Rate : 0.4348         
    P-Value [Acc > NIR] : 0.0007403      
                                         
                  Kappa : 0.6395         
                                         
 Mcnemar's Test P-Value : NA             

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.8889   0.9000  0.25000
Specificity            1.0000   0.6923  0.94737
Pos Pred Value         1.0000   0.6923  0.50000
Neg Pred Value         0.9333   0.9000  0.85714
Prevalence             0.3913   0.4348  0.17391
Detection Rate         0.3478   0.3913  0.04348
Detection Prevalence   0.3478   0.5652  0.08696
Balanced Accuracy      0.9444   0.7962  0.59868
[1] "MD -4"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D 13  8  5
         L  4 16  3
         W  0  1  1

Overall Statistics
                                          
               Accuracy : 0.5882          
                 95% CI : (0.4417, 0.7242)
    No Information Rate : 0.4902          
    P-Value [Acc > NIR] : 0.1036          
                                          
                  Kappa : 0.3161          
                                          
 Mcnemar's Test P-Value : 0.0620          

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.7647   0.6400  0.11111
Specificity            0.6176   0.7308  0.97619
Pos Pred Value         0.5000   0.6957  0.50000
Neg Pred Value         0.8400   0.6786  0.83673
Prevalence             0.3333   0.4902  0.17647
Detection Rate         0.2549   0.3137  0.01961
Detection Prevalence   0.5098   0.4510  0.03922
Balanced Accuracy      0.6912   0.6854  0.54365
[1] "MD -2"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  4  2  1
         L  1 13  3
         W  0  3  4

Overall Statistics
                                          
               Accuracy : 0.6774          
                 95% CI : (0.4863, 0.8332)
    No Information Rate : 0.5806          
    P-Value [Acc > NIR] : 0.1820          
                                          
                  Kappa : 0.4504          
                                          
 Mcnemar's Test P-Value : 0.7212          

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.8000   0.7222   0.5000
Specificity            0.8846   0.6923   0.8696
Pos Pred Value         0.5714   0.7647   0.5714
Neg Pred Value         0.9583   0.6429   0.8333
Prevalence             0.1613   0.5806   0.2581
Detection Rate         0.1290   0.4194   0.1290
Detection Prevalence   0.2258   0.5484   0.2258
Balanced Accuracy      0.8423   0.7073   0.6848
[1] "MD -3"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  4  2  1
         L  6 11  6
         W  2  4  6

Overall Statistics
                                          
               Accuracy : 0.5             
                 95% CI : (0.3419, 0.6581)
    No Information Rate : 0.4048          
    P-Value [Acc > NIR] : 0.1359          
                                          
                  Kappa : 0.2215          
                                          
 Mcnemar's Test P-Value : 0.4346          

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity           0.33333   0.6471   0.4615
Specificity           0.90000   0.5200   0.7931
Pos Pred Value        0.57143   0.4783   0.5000
Neg Pred Value        0.77143   0.6842   0.7667
Prevalence            0.28571   0.4048   0.3095
Detection Rate        0.09524   0.2619   0.1429
Detection Prevalence  0.16667   0.5476   0.2857
Balanced Accuracy     0.61667   0.5835   0.6273
[1] "MD -7"
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
Something is wrong; all the Accuracy metric values are missing:
    Accuracy       Kappa    
 Min.   : NA   Min.   : NA  
 1st Qu.: NA   1st Qu.: NA  
 Median : NA   Median : NA  
 Mean   :NaN   Mean   :NaN  
 3rd Qu.: NA   3rd Qu.: NA  
 Max.   : NA   Max.   : NA  
 NA's   :1     NA's   :1    
<simpleError: Stopping>
