[1] "MD -1"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  0  0
         L 19 65 31
         W  0  0  1

Overall Statistics
                                          
               Accuracy : 0.569           
                 95% CI : (0.4738, 0.6606)
    No Information Rate : 0.5603          
    P-Value [Acc > NIR] : 0.4643          
                                          
                  Kappa : 0.025           
                                          
 Mcnemar's Test P-Value : NA              

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.0000  1.00000 0.031250
Specificity            1.0000  0.01961 1.000000
Pos Pred Value            NaN  0.56522 1.000000
Neg Pred Value         0.8362  1.00000 0.730435
Prevalence             0.1638  0.56034 0.275862
Detection Rate         0.0000  0.56034 0.008621
Detection Prevalence   0.0000  0.99138 0.008621
Balanced Accuracy      0.5000  0.50980 0.515625
[1] "MD"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  0  0  1
         L 13 47 23
         W  3 12  6

Overall Statistics
                                          
               Accuracy : 0.5048          
                 95% CI : (0.4055, 0.6038)
    No Information Rate : 0.5619          
    P-Value [Acc > NIR] : 0.8991304       
                                          
                  Kappa : 0.004           
                                          
 Mcnemar's Test P-Value : 0.0005691       

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity          0.000000   0.7966  0.20000
Specificity          0.988764   0.2174  0.80000
Pos Pred Value       0.000000   0.5663  0.28571
Neg Pred Value       0.846154   0.4545  0.71429
Prevalence           0.152381   0.5619  0.28571
Detection Rate       0.000000   0.4476  0.05714
Detection Prevalence 0.009524   0.7905  0.20000
Balanced Accuracy    0.494382   0.5070  0.50000
[1] "MD -5"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction D L W
         D 9 1 1
         L 0 9 3
         W 0 0 0

Overall Statistics
                                         
               Accuracy : 0.7826         
                 95% CI : (0.563, 0.9254)
    No Information Rate : 0.4348         
    P-Value [Acc > NIR] : 0.0007403      
                                         
                  Kappa : 0.629          
                                         
 Mcnemar's Test P-Value : 0.1717971      

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            1.0000   0.9000   0.0000
Specificity            0.8571   0.7692   1.0000
Pos Pred Value         0.8182   0.7500      NaN
Neg Pred Value         1.0000   0.9091   0.8261
Prevalence             0.3913   0.4348   0.1739
Detection Rate         0.3913   0.3913   0.0000
Detection Prevalence   0.4783   0.5217   0.0000
Balanced Accuracy      0.9286   0.8346   0.5000
[1] "MD -4"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  4  2  0
         L 13 19  6
         W  0  4  3

Overall Statistics
                                         
               Accuracy : 0.5098         
                 95% CI : (0.366, 0.6525)
    No Information Rate : 0.4902         
    P-Value [Acc > NIR] : 0.444          
                                         
                  Kappa : 0.142          
                                         
 Mcnemar's Test P-Value : NA             

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity           0.23529   0.7600  0.33333
Specificity           0.94118   0.2692  0.90476
Pos Pred Value        0.66667   0.5000  0.42857
Neg Pred Value        0.71111   0.5385  0.86364
Prevalence            0.33333   0.4902  0.17647
Detection Rate        0.07843   0.3725  0.05882
Detection Prevalence  0.11765   0.7451  0.13725
Balanced Accuracy     0.58824   0.5146  0.61905
[1] "MD -2"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  3  2  0
         L  2 14  1
         W  0  2  7

Overall Statistics
                                         
               Accuracy : 0.7742         
                 95% CI : (0.589, 0.9041)
    No Information Rate : 0.5806         
    P-Value [Acc > NIR] : 0.02006        
                                         
                  Kappa : 0.6111         
                                         
 Mcnemar's Test P-Value : NA             

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity           0.60000   0.7778   0.8750
Specificity           0.92308   0.7692   0.9130
Pos Pred Value        0.60000   0.8235   0.7778
Neg Pred Value        0.92308   0.7143   0.9545
Prevalence            0.16129   0.5806   0.2581
Detection Rate        0.09677   0.4516   0.2258
Detection Prevalence  0.16129   0.5484   0.2903
Balanced Accuracy     0.76154   0.7735   0.8940
[1] "MD -3"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
--> [ 3 ] Predicting on test data
--> [ 4 ] Confusion matrix
Confusion Matrix and Statistics

          Reference
Prediction  D  L  W
         D  8  4  7
         L  4 13  6
         W  0  0  0

Overall Statistics
                                          
               Accuracy : 0.5             
                 95% CI : (0.3419, 0.6581)
    No Information Rate : 0.4048          
    P-Value [Acc > NIR] : 0.135931        
                                          
                  Kappa : 0.2297          
                                          
 Mcnemar's Test P-Value : 0.004637        

Statistics by Class:

                     Class: D Class: L Class: W
Sensitivity            0.6667   0.7647   0.0000
Specificity            0.6333   0.6000   1.0000
Pos Pred Value         0.4211   0.5652      NaN
Neg Pred Value         0.8261   0.7895   0.6905
Prevalence             0.2857   0.4048   0.3095
Detection Rate         0.1905   0.3095   0.0000
Detection Prevalence   0.4524   0.5476   0.0000
Balanced Accuracy      0.6500   0.6824   0.5000
[1] "MD -7"
--> [ 0 ] Transform data with PCA
--> [ 1 ] Configuring cross validation
--> [ 2 ] Training model
Something is wrong; all the Accuracy metric values are missing:
    Accuracy       Kappa    
 Min.   : NA   Min.   : NA  
 1st Qu.: NA   1st Qu.: NA  
 Median : NA   Median : NA  
 Mean   :NaN   Mean   :NaN  
 3rd Qu.: NA   3rd Qu.: NA  
 Max.   : NA   Max.   : NA  
 NA's   :3     NA's   :3    
<simpleError: Stopping>
