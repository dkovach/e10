#!/usr/bin/Rscript

###########################################################################
#
# name			: analysis.r
#
# purpose		: study google docs
#
# usage			: Rscript analysis.r
#
# description		:
#
###########################################################################

libraries_needed = c( 
	"caret", 
	"optparse",
	"rpart", 
	"rpart.plot", 
	"scatterplot3d" )

for ( lib in libraries_needed )
{
	if ( ! ( lib %in% rownames( installed.packages() ) ) )
	{
		install.packages( lib )
	}
}


suppressMessages( library( "caret" ) )
suppressMessages( library( "optparse" ) )
suppressMessages( library( "rpart" ) )
suppressMessages( library( "rpart.plot" ) )
suppressMessages( library( "scatterplot3d" ) )


get_df <- function()
{
	df <- read.csv( "Data/data.csv", stringsAsFactors=F )
	df$Result[ which( df$Result == 'W' ) ] <- 1
	df$Result[ which( df$Result == 'D' ) ] <- 0
	df$Result[ which( df$Result == 'L' ) ] <- -1
	col_omit <- c( 1, 4 )
	return_value <- df[ , -col_omit ]
	return( return_value )
}

get_df_class <- function()
{
	df <- read.csv( "Data/data.csv", stringsAsFactors=F )
	col_omit <- c( 1, 4 )
	return_value <- df[ , -col_omit ]
	return( return_value )
}

do_pca_graph <- function()
{
	df_all <- get_df()
	for ( MDTag in unique( df_all$MDTag ) )
	{
		print( MDTag )
		df <- df_all[ which( df_all$MDTag == MDTag ), ]
		cnames = colnames( df )
		dff <- df[ , -which( cnames %in% c( "Result", "MDTag" ) ) ]
		pf <- prcomp( dff, scale. = T )
		print( summary( pf ) )
		cols <- rep( "red", nrow( df ) )
		cols[ which( df$Result == 1 ) ] <- "green"
		cols[ which( df$Result == 0 ) ] <- "yellow"

		png( paste0( "Graphs/pca_md_", MDTag, "_biplot.png" ), res=500, height=5000, width=9000 )
		biplot( pf )
		dev.off()

		pc_var <- ( pf$sdev ^ 2 ) / sum( pf$sdev ^ 2 )
		png( paste0( "Graphs/pca_md_", MDTag, "_screeplot.png" ), res=500, height=5000, width=9000 )
		par( mfrow=c( 2, 1 ) )
		plot( pc_var, main="Variance of PC's", 
			xlab = "Principal Component", ylab = "Proportion of Variance Explained", type = "b")
		plot( cumsum( pc_var ), main="Cumulative Sum of Variance of PC's", 
			xlab = "Principal Component", ylab = "Cumulative Sum", type = "b")
		dev.off()

		png( paste0( "Graphs/pca_md_", MDTag, "_2D.png" ), res=500, height=5000, width=9000 )
		plot( pf$x[ , 1 : 2 ], main="PCA", xlab="PC1", ylab="PC2", col=cols, lwd=2 )
		legend( "topright", legend=c( "Win", "Draw", "Loss" ), col=c( "green", "yellow", "red" ), lwd=rep( 3, 3 ) )
		dev.off()

		png( paste0( "Graphs/pca_md_", MDTag, "_3D.png" ), res=500, height=5000, width=9000 )
		scatterplot3d( pf$x[ , 1 : 3 ], main="PCA 3D", xlab="PC1", ylab="PC2", zlab="PC3", color=cols, pch=16 )
		legend( "topright", legend=c( "Win", "Draw", "Loss" ), col=c( "green", "yellow", "red" ), lwd=rep( 3, 3 ) )
		dev.off()
	}
}


do_tree_graph <- function()
{
	df_all <- get_df_class()
	for ( MDTag in unique( df_all$MDTag ) )
	{
		print( MDTag )
		df <- df_all[ which( df_all$MDTag == MDTag ), ]
		cnames = colnames( df )
		dff <- df[ , -which( cnames %in% c( "Result", "MDTag" ) ) ]
		pf <- prcomp( dff, scale. = T )
		train.data  <- data.frame( "Result"=df$Result, pf$x[ , 1 : 3 ] )
		print( summary( train.data ) )
		tree.model <- rpart( Result ~ ., data=train.data, cp=0.2 )
		png( paste0( "Graphs/tree_md_", MDTag, ".png" ), res=500, height=5000, width=9000 )
		rpart.plot( tree.model, box.palette="RdBu", shadow.col="gray", nn=T )
		dev.off()
	}
}


train_and_classify <- function( pca_transform=T, method_name="gbm" )
{
	df_all <- get_df_class()
	set.seed( 998 )
	for ( MDTag in unique( df_all$MDTag ) )
	{
		print( MDTag )
		df <- df_all[ which( df_all$MDTag == MDTag ), ]
		cnames = colnames( df )
		dff <- df[ , -which( cnames == "MDTag" ) ]
		training_indices <- createDataPartition( dff$Result, p = .75, list = FALSE)
		training_data <- dff[ training_indices, ]
		testing_data  <- dff[ - training_indices, ]

		if ( pca_transform )
		{
			cat( "--> [ 0 ] Transform data with PCA\n" )
			training_result = training_data$Result
			testing_result = testing_data$Result
			pca <- prcomp( training_data[ , 2 : ncol( training_data ) ], scale. = T )
			training_data = pca$x
			testing_data <- predict( pca, newdata=testing_data[ , 2 : ncol( testing_data ) ] )
			training_data <- cbind( data.frame( "Result"=training_result, training_data[ , 1 : 3 ] ) )
			testing_data <- cbind( data.frame( "Result"=testing_result, testing_data[ , 1 : 3 ] ) )
		}

		tryCatch( {
			cat( "--> [ 1 ] Configuring cross validation\n" )
			fit_control <- trainControl(
				method = "repeatedcv",
				number = 10,
				repeats = 10
			)

			cat( "--> [ 2 ] Training model\n" )
			model_fit <- train(
				Result ~ ., data = training_data, 
				method = method_name, 
				trControl = fit_control,
				verbose = F 
			)

			cat( "--> [ 3 ] Predicting on test data\n" )
			gbm_predict <- predict(
				model_fit, 
				newdata = testing_data 
			)

			cat( "--> [ 4 ] Confusion matrix\n" )
			print( confusionMatrix( gbm_predict, as.factor( testing_data$Result ) ) )
		}, error=function( e ) {
			print( e )
		} )

	}
}

###########################################################################
# main - where the magic happens
###########################################################################

option_list <- list(
      make_option( c( "-e", "--explore"),  action="store_true", dest="explore", default=FALSE, help="Explore data with PCA"   ),
      make_option( c( "-r", "--run"),      action="store_true", dest="run",     default=FALSE, help="Run classification"      ),
      make_option( c( "-p", "--pca"),      action="store_true", dest="pca",     default=FALSE, help="Transform data with PCA" ),
      make_option( c( "-m", "--method"  ), type="character",    dest="method",  default="gbm", help="Classification method"   )
)
parser <- OptionParser( usage="%prog [options] file", option_list=option_list)
args   <- parse_args( parser )


if ( args$explore )
{
	do_pca_graph()
}

if ( args$run )
{
	train_and_classify( pca_transform=args$pca, method_name=args$method )
}
